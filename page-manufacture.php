<?php get_header(); ?>
	
    <div class="container my-5">
      <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
      <div class="row">
        <div class="col-md-12"><h1><?=the_title()?></h1></div>
      </div>
      <div class="row mt-3">
        <?php $res = get_posts( ['numberposts'=>30, 'post_type'=>'manuf', 'order_by'=>'id', 'order'=>'asc'] ); ?>
        <?php foreach ( $res as $item ) { ?>
        <div class="col-md-4 mb-3"><img src="<?=get_the_post_thumbnail_url($item->ID)?>" style="width: 100%;" /></div>
        <?php } // foreach ?>
      </div>
      <?php endwhile; endif; ?>
    </div>
    
<?php get_footer(); ?>