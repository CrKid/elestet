<?php get_header(); ?>

	 <div class="container my-5">
      <div class="row">
        <div class="col-md-<?=((is_front_page())?'7':'12')?>">
          <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
          <h1><?=the_title()?></h1>
          	<?php the_content(); ?>
          <?php endwhile; ?>
          <?php endif; ?>
          
        </div>
        <?php if ( is_front_page() ) { ?>
        <div class="col-md-5">
          <img src="assets/images/main/01.jpg" style="width: 100%" /><br /><br />
          <img src="assets/images/main/02.jpg" style="width: 100%" />
        </div>
        <? } // if ?>
      </div>
    </div>
    
    <?php if ( is_front_page() ) { ?>
    <div class="projects-title">
      <div class="container">
        <div class="py-3 px-5 title"><h4><strong>Избранные проекты</strong></h4></div>
      </div>
    </div>
    
    <div class="container my-5">
      <div class="row">
        <div class="col-md-3 text-center">
          <img src="/assets/images/projects/01-1.jpg" style="width: 100%" />
          <h5 class="mt-3"><strong>Жилой дом г. Химки </strong></h5>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
        <div class="col-md-3 text-center">
          <img src="/assets/images/projects/02-1.jpg" style="width: 100%" />
          <h5 class="mt-3"><strong>Жилой дом г. Химки </strong></h5>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
        <div class="col-md-3 text-center">
          <img src="/assets/images/projects/03-1.jpg" style="width: 100%" />
          <h5 class="mt-3"><strong>Жилой дом г. Химки </strong></h5>
          <a href="#" class="btn btn-primary">Подробнее</a>
        </div>
        <div class="col-md-3 text-center">
          <img src="/assets/images/projects/04-1.jpg" style="width: 100%" />
          <h5 class="mt-3"><strong>Жилой дом г. Химки </strong></h5>
          <a href="#" class="btn btn-primary btn-block">Подробнее</a>
        </div>
      </div>
    </div>
    
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-center">НАШИ ПАРТНЕРЫ</h2>
        </div>
      </div>
    </div>
    <?php } // if ?>
    
<?php get_footer(); ?>
