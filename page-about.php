<?php get_header(); ?>
	
    <div class="container my-5">
      <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
      <div class="row text-justify">
        <div class="col-md-12"><h1><?=the_title()?></h1></div>
        <div class="col-md-12">
          <img src="<?=get_the_post_thumbnail_url()?>" style="width: 100%;" />
          <p class="mt-3"><?=get_post_custom()['description'][0]?></p>
          <?php the_content(); ?>
        </div>
        <div class="col-md-12">
          <h2 class="my-2">Реквизиты компании ООО "Производственная компания "ЭлектроЭстетика"</h2>
          <?=get_post_custom()['requisites'][0]?>
        </div>
      </div>
      <?php endwhile; endif; ?>
    </div>
    
<?php get_footer(); ?>