	<?php $res = get_posts( ['numberposts'=>100, 'post_type'=>'slider', 'order_by'=>'id', 'order'=>'asc'] ); ?>
	<div class="slider">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php foreach ( $res as $item ) { ?>
          <div class="swiper-slide" style="background: url(<?=get_the_post_thumbnail_url($item->ID)?>) center center no-repeat;">
            <div class="container">
              <div class="row">
                <div class="col-md-4 caption p-4 mt-5">
                  <h4><strong><?=$item->post_title?></strong></h4>
                  <p><small><?=$item->post_content?></small></p>
                </div>
              </div>
            </div>
          </div>
          <? } // foreach ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination swiper-pagination-white"></div>
        <div class="swiper-button-next swiper-button-white"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
      </div>
    </div>