<?php get_header(); ?>

	<div class="container my-5">
      <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
      
      <div class="row">
        <div class="col-md-12"><h1><?=the_title()?></h1></div>
      </div>
      
      <div class="row">
		<div class="col-md-4">
          <ul class="p-0" style="list-style: none;">
            <?php foreach ( array_reverse($GLOBALS['contacts']) as $item) { ?>
            <li class="py-1"><a href="<?=get_post_custom($item->ID)['link'][0]?>"><i class="fa fa-<?=get_post_custom($item->ID)['icon'][0]?>" aria-hidden="true"></i> <?=$item->post_content?></a></li>
            <?php } // foreach ?>
          </ul>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="Email" placeholder="Ваш email *" required>
          </div>
          <div class="form-group">
            <input type="phone" class="form-control" name="Phone" placeholder="Ваш телефон *" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <textarea class="form-control" name="Comment" rows="6" required></textarea>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <div class="form-group">
            <input type="hidden" class="form-control" name="Form" value="Обратная связь">
            <a href="#" class="btn btn-secondary" role="Send">Отправить</a> <span role="spinner"><i class="fa fa-spinner fa-spin fa-fw"></i></span>
          </div>
        </div>
        <div class="col-md-4"></div>
      </div>
      
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
          <div class="alert alert-dismissible alert-success">
            <strong>Спасибо за обращение!</strong> Мы свяжемся с Вами в ближайшее время.
          </div>
          <div class="alert alert-dismissible alert-danger">
            <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
          </div>
        </div>
      </div>
      
      <div class="row mt-3">
        <?=get_post_custom()['map'][0]?>
      </div>
      
      <?php endwhile; endif; ?>
    </div>
    
<?php get_footer(); ?>