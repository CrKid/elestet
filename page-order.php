﻿<?php get_header(); ?>
	
    <div class="container my-5">
      <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
      <div class="row text-justify">
        
        <div class="col-md-12"><h1><?=the_title()?></h1></div>
        
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="Email" placeholder="Ваш email *" required>
          </div>
          <div class="form-group">
            <input type="phone" class="form-control" name="Phone" placeholder="Ваш телефон *" required>
          </div>
          <div class="form-group">
            <select class="form-control" name="Product" required>
              <option>Выберите продукцию *</option>
              <?php $res = get_posts( ['numberposts'=>50, 'category'=>9, 'order_by'=>'id', 'order'=>'asc'] ); ?>
              <?php foreach ( $res as $item ) { ?>
              <option value="<?=$item->post_title?>" <?=(((int)$_GET['product']==$item->ID)?'selected':'')?>><?=$item->post_title?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <textarea class="form-control" name="Comment" rows="8" required></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <input type="hidden" class="form-control" name="Form" value="Заявка на продукцию">
            <a href="#" class="btn btn-secondary" role="Send">Отправить</a> <span role="spinner"><i class="fa fa-spinner fa-spin fa-fw"></i></span>
          </div>
        </div>
      
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-dismissible alert-success">
            <strong>Спасибо за обращение!</strong> Мы свяжемся с Вами в ближайшее время.
          </div>
          <div class="alert alert-dismissible alert-danger">
            <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
          </div>
        </div>
      </div>
      
      <?php endwhile; endif; ?>
    </div>
    
<?php get_footer(); ?>