<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	require_once $_SERVER['DOCUMENT_ROOT'].'/wp-load.php';
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	require 'vendor/autoload.php';
	
	// BApp::sp( get_post_custom(114)['recipients'] );
	
	if ( $_POST ) {
		
		$mail = new PHPMailer();
		
		try {
			
			$mail->CharSet = 'utf-8';
			$mail->isHTML(true); 
			
			$mail->setFrom('mailer@elestet.ru', 'FormSender Elestet');
			
			foreach ( get_post_custom(153)['recipients'] as $r ) $mail->addAddress($r, '');
			
			$mail->Subject = 'Заполнена форма '.$_POST['Form'].', '.date('Y-m-d H:i').'.';
			$mail->Body = 'Имя: '.$_POST['Name'].'<br />';
			$mail->Body .= 'Email: '.$_POST['Email'].'<br />';
			$mail->Body .= 'Телефон: '.$_POST['Phone'].'<br /><br />';
			if ( $_POST['Product'] ) $mail->Body .= 'Продукция: '.$_POST['Product'].'<br /><br />';
			$mail->Body .= 'Сообщение:<br />'.$_POST['Comment'].'<br /><br /><br />';
			$mail->Body .= 'Форма: '.$_POST['Form'].'<br /><br />';
			$mail->Body .= 'Источник: <a href="'.$_POST['Source'].'" target="_blank">'.$_POST['source'].'</a>';
			
			$mail->send();
			
			$arIns = [
				'post_title'    => $mail->Subject,
				'post_content'  => $mail->Body,
				'post_status'   => 'private',
				'post_author'   => 1,
				'post_type' 	=> 'feedback'
			];
	
			$id = wp_insert_post( $arIns );
			
			echo json_encode(['status'=>'success', 'message'=>'Post ID: '.$id]);
		
		} catch ( Exception $e ) {
			
			echo json_encode(['status'=>'error', 'message'=>'Mailer Error: '.$mail->ErrorInfo]);
		}
		
	} else {
		
		echo json_encode(['status'=>'error', 'message'=>'Error: No POST data']);
	
	} // if
	
?>