'use strict';

function checkEmail(email) {

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|("\.+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


$('input[type="phone"]').inputmask({'mask': '+7 (999) 999-99-99', showMaskOnHover: false});

var swiper = new Swiper('.swiper-container', {
  navigation: {
	nextEl: '.swiper-button-next',
	prevEl: '.swiper-button-prev',
  },
  pagination: {
	el: '.swiper-pagination',
	dynamicBullets: true,
  },
  loop: true,
  autoplay: {
	delay: 3500,
	disableOnInteraction: false,
  },
});

$('[role="menu"]').click( function() {
	
	$('[role="top-menu"]').slideToggle(300);
});


$('a[role="Send"]').click( function() {
	
	var b = $(this);
	
	if ( !$(b).hasClass('disabled') ) {
		
		var Send = {};
		Send.Status = true;
		Send.Form = $('input[name="Form"]').val();
		Send.Source = location.href;
		
		$('input[name="Name"], input[name="Email"], input[name="Phone"], textarea[name="Comment"], select[name="Product"]').each( function(i, e) {
			
			if ( $(e).val() ) {
				
				Send[$(e).attr('name')] = $(e).val();
				$(e).removeClass('is-invalid');
			
			} else {
				
				Send.Status = false;
				$(e).addClass('is-invalid');
			}
		});
		
		if ( Send.Email && checkEmail(Send.Email) ) {
			
			$('input[name="Email"]').removeClass('is-invalid');
			
		} else {
			
			Send.Status = false;
			$('input[name="Email"]').addClass('is-invalid');
		}
		
		if ( Send.Status ) {
			
			$(b).addClass('disabled').parent().children('span[role="spinner"]').show();
			$.ajax({

				type: 'POST',
				url: '/ajax/',
				data: Send,
				success: function(data) {
					
					var res = JSON.parse( data );
					var d = ( res.status == 'success' ) ? $('.alert-success') : $('.alert-danger');
					
					if ( typeof window.yaCounter46947945 != 'undefined' ) yaCounter46947945.reachGoal('SendForm');
                    if ( typeof window.ga != 'undefined' ) ga('send', 'event', 'Форма Обратной связи', 'Отправлено');
					
					$(d).slideDown(300);
				},
	
				error: function() {
					
					$('.alert-danger').slideDown(300);
				}
			});
			
			$(b).removeClass('disabled').parent().children('span[role="spinner"]').hide();
		}
	
	} // if not disabled
	
	return false;
});
