<?php get_header(); ?>
    
    <div class="container my-5">
      
      <div class="row mt-3">
        <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
        <div class="col-md-9">
          <div class="row">
            <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
            <div class="col-md-6">
              <h3><strong><?=the_title()?> (<?=get_field('abbr')?>)</strong></h3>
              <?=the_excerpt()?>
              <a href="/order/?product=<?=the_ID()?>" class="btn btn-info btn-block">Оставить заявку</a>
            </div>
            <div class="col-md-6"><img src="<?=get_the_post_thumbnail_url($item->ID)?>" style="width: 100%;" /></div>
            <div class="w-100"></div>
            <hr />
            <div class="col-md-12 mt-3"><?=the_content()?></div>
            <?php $res = get_field('assoc'); ?>
			<?php if ( !empty($res) ) { ?>
            <div class="col-md-12 mt-3">Смотрите так же: 
              <?php foreach ( $res as $k => $item ) { ?>
              <a href="/products/<?=$item->ID?>/"><?=$item->post_title?></a><?=(( count($res) > 1 && $k <= count($res)-1 )?', ':'')?>
              <?php } ?>
            </div>
            <?php } // if ?>
            <div class="col-md-6"> <a href="/order/?product=<?=the_ID()?>" class="btn btn-info btn-block">Оставить заявку</a></div>
            <?php endwhile; endif; ?>
            
          </div>
          
        </div>
      </div>
    </div>
    
<?php get_footer(); ?>