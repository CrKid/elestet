<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Boretscy A">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css" />

    <link href="/assets/css/app.css?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/assets/css/app.css')?>" rel="stylesheet">
    
    <title><?=((is_category())?single_cat_title():the_title())?> | ООО "Производственная компания "ЭлектроЭстетика"</title>
    <meta name="description" content="<?=get_post_custom()['description'][0]?>">
    <meta name="keywords" content="<?=get_post_custom()['keywords'][0]?>">
    
  </head>

  <body>
	
    <?php include __DIR__.'/include/_counters.php'; ?>
    <?php include __DIR__.'/include/_settings.php'; ?>
    
    <div class="top-row">
      <div class="container py-3">
        <div class="col-md-12 is-mob"><a href="#" role="menu"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
        <div class="col-md-12" role="top-menu">
          <ul>
            <?php foreach ( $GLOBALS['menu'] as $item ) { ?>
            <li class="py-2 pr-2"><a href="<?=get_post_custom($item->ID)['link'][0]?>"><?=$item->post_title?></a></li>
            <?php } // foreach ?>
          </ul>
          <div class="clear"></div>
        </div>
        <hr role="top-menu" />
        <?php foreach ( $GLOBALS['top_contacts'] as $item ) { ?>
        <div class="col-md-12 pt-2" role="top-menu">
          <?=$item->post_title?>: <strong><a href="<?=get_post_custom($item->ID)['link'][0]?>"><?=$item->post_content?></a></strong>
        </div>
        <? } // foreach ?>
      </div>
    </div>
    
    <div class="container">
      <div class="row logo-row py-4">
        <div class="col-md-6">
          <a href="/">
            <svg class="svg" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo"></use></svg>
          </a>
        </div>
        <?php foreach ( $GLOBALS['top_contacts'] as $item ) { ?>
        <div class="col-md-2 pt-2 is-pc">
          <p><?=$item->post_title?>:<br /><strong><a href="<?=get_post_custom($item->ID)['link'][0]?>"><?=$item->post_content?></a></strong></p>
        </div>
        <? } // foreach ?>
      </div>
    </div>
    
    <?php include __DIR__.'/menu.php'; ?>
    
    <?php if ( is_front_page() ) { include __DIR__.'/slider.php'; } else { ?>
        <div class="banner">
          <div class="container">
            <div class="row">
              <div class="col-md-12 mt-5">
                <h1 class="mt-5"><strong><?=((is_category())?single_cat_title():the_title())?></strong></h1>
              </div>
            </div>
          </div>
        </div>
    <?php } // if ?>