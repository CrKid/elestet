<?php
	
	class BApp {
		
		// DEBUG
		public static function sp($q, $h = false, $t = false) {
			
			echo '<pre '.(($h)?'style="display: none;"':'').'>'; if ($t) echo $t.'<br />'; print_r($q); echo '</pre>';
		}

		public static function sd($q, $h = false, $t = false) {
			
			echo '<pre '.(($h)?'style="display: none;"':'').'>'; if ($t) echo $t.'<br />'; var_dump($q); echo '</pre>';
		}
		
		
		public static function formatPhoneIn( $phone ) {
			
			// +7 (111) 111-11-11 -> 71111111111
			
			$phone = preg_replace('/[^0-9]/', '', $phone);
			$phone = mb_substr($phone, 0, 11);
			
			if ( mb_strlen($phone) == 10 ) $phone = '7'.$phone;
			if ( mb_strlen($phone) == 7 ) $phone = '7861'.$phone;
			if ( $phone[0] == '8' ) $phone = '7'.mb_substr($phone, 1);
			
			return $phone;
		}
		
		public static function formatPhoneOut( $str ) {
			
			// 71111111111 -> +7 (111) 111-11-11
			
			$str = self::formatPhoneIn( $str );
			
			for ($k = 0; $k < mb_strlen((string)$str); $k++) $phone[] = mb_substr($str, $k, 1);
			return '+'.$phone[0].' ('.$phone[1].$phone[2].$phone[3].') '.$phone[4].$phone[5].$phone[6].'-'.$phone[7].$phone[8].'-'.$phone[9].$phone[10];
		}
	}
	
	function remove_menus(){
	  remove_menu_page( 'edit-comments.php' );          //Комментарии
	  remove_menu_page( 'themes.php' );                 //Внешний вид
	  remove_menu_page( 'plugins.php' );                //Плагины
	  remove_menu_page( 'tools.php' );                  // Инструменты
	}
	
	
	
	add_theme_support( 'post-thumbnails', ['post', 'slider', 'manuf', 'certs', 'page'] );
	add_action( 'admin_menu', 'remove_menus' );

?>