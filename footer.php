	<div class="footer py-5 mt-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?=$GLOBALS['footer']->post_content;?>
          </div>
          <div class="col-md-3">
            <div class="title pb-2">Меню</div>
            <ul class="p-0 pt-2">
              <?php foreach ( $GLOBALS['menu'] as $item ) { ?>
              <li class="pt-2"><a href="<?=get_post_custom($item->ID)['link'][0]?>"><?=$item->post_title?></a></li>
              <?php } // foreach ?>
            </ul>
          </div>
          <div class="col-md-3">
            <div class="title pb-2">Контакты</div>
            <ul class="p-0 pt-2">
              <?php foreach ( array_reverse($GLOBALS['contacts']) as $item ) { ?>
              <li class="pt-2"><a href="<?=get_post_custom($item->ID)['link'][0]?>"><i class="fa fa-<?=get_post_custom($item->ID)['icon'][0]?>" aria-hidden="true"></i> <?=$item->post_content?></a></li>
              <?php } // foreach ?>
            </ul>
          </div>
          <div class="col-md-3">
            <div class="title pb-2">Полезная информация</div>
          </div>
        </div>
      </div>
    </div>
    
	<? include __DIR__.'/include/_svg.php'; ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
    
    <script src="/assets/js/app.js?<?=md5_file($_SERVER['DOCUMENT_ROOT'].'/assets/js/app.js')?>"></script>
    
    <? include __DIR__.'/include/_scripts.php'; ?>
    
  </body>
</html>
