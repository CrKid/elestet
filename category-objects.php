<?php get_header(); ?>
	
    <div class="container my-5">
      <div class="row">
        <div class="col-md-12"><h1><?=single_cat_title()?></h1></div>
      </div>
      <div class="row mt-3">
        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
        <div class="col-md-4 mb-3">
          <img src="<?=get_the_post_thumbnail_url()?>" style="width: 100%;" />
          <h5 class="mt-3"><strong><?=the_title()?> </strong></h5>
        </div>
        <?php endwhile; endif; ?>
      </div>
    </div>
    
<?php get_footer(); ?>