<?php get_header(); ?>

	 <div class="container my-5">
      <div class="row">
        <div class="col-md-7">
          <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
          <h1><?=the_title()?></h1>
          	<?php the_content(); ?>
          <?php endwhile; ?>
          <?php endif; ?>
          
        </div>
        <div class="col-md-5">
          <img src="assets/images/main/01.jpg" style="width: 100%" /><br /><br />
          <img src="assets/images/main/02.jpg" style="width: 100%" />
        </div>
      </div>
    </div>
    
    <div class="projects-title">
      <div class="container">
        <div class="py-3 px-5 title"><h4><strong>Избранные проекты</strong></h4></div>
      </div>
    </div>
    
    <?php $res = get_posts( ['numberposts'=>4, 'category'=>10] ); ?>
    <div class="container my-5">
      <div class="row">
        
		<?php foreach ( $res as $item ) { ?>
        <div class="col-md-3 text-center">
          <img src="<?=get_the_post_thumbnail_url($item->ID)?>" style="width: 100%" />
          <h5 class="mt-3"><strong><?=$item->post_title?> </strong></h5>
        </div>
        <? } ?>
        
      </div>
    </div>
    
    <?php /*
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-center">НАШИ ПАРТНЕРЫ</h2>
        </div>
      </div>
    </div>
	*/ ?>
    
<?php get_footer(); ?>
