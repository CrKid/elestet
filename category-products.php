<?php get_header(); ?>
    
    <div class="container my-5">
      
      <div class="row mt-3">
        <div class="col-md-3 sidebar"><?php get_sidebar(); ?></div>
        <div class="col-md-9">
          <div class="row">
            <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
            <div class="col-md-4 mb-3">
              <a href="/prodicts/<?=the_ID()?>/"><img class="pb-2" src="<?=get_the_post_thumbnail_url($item->ID)?>" style="width: 100%;" /></a>
              <h6 class="pb-2 text-center"><a href="/prodicts/<?=the_ID()?>/"><strong><?=the_title()?></strong></a></h6>
              <a href="/prodicts/<?=the_ID()?>/" class="btn btn-secondary btn-block">Подробнее</a>
            </div>
            <?php endwhile; endif; ?>
          </div>
          
        </div>
      </div>
    </div>
    
<?php get_footer(); ?>